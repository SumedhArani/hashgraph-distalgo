# -*- generated by 1.0.12 -*-
import da
PatternExpr_1812 = da.pat.TuplePattern([da.pat.ConstantPattern('gossip'), da.pat.FreePattern('gossip_data'), da.pat.FreePattern('run_time'), da.pat.FreePattern('gossiper')])
PatternExpr_2017 = da.pat.TuplePattern([da.pat.ConstantPattern('gossip_rcvd')])
PatternExpr_2037 = da.pat.TuplePattern([da.pat.ConstantPattern('gossip_rcvd')])
PatternExpr_2065 = da.pat.TuplePattern([da.pat.ConstantPattern('done')])
PatternExpr_2070 = da.pat.BoundPattern('_BoundPattern2072_')
PatternExpr_2022 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.FreePattern(None)]), da.pat.TuplePattern([da.pat.ConstantPattern('gossip_rcvd')])])
PatternExpr_2042 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.FreePattern(None)]), da.pat.TuplePattern([da.pat.ConstantPattern('gossip_rcvd')])])
PatternExpr_2073 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.BoundPattern('_BoundPattern2079_')]), da.pat.TuplePattern([da.pat.ConstantPattern('done')])])
PatternExpr_2177 = da.pat.TuplePattern([da.pat.ConstantPattern('done'), da.pat.BoundPattern('_BoundPattern2180_')])
PatternExpr_2183 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.FreePattern(None)]), da.pat.TuplePattern([da.pat.ConstantPattern('done'), da.pat.BoundPattern('_BoundPattern2193_')])])
_config_object = {'channel': 'fifo'}
import sys
import random
import datetime
import time
import hashlib
import copy
process_mapper = {}

class Data():

    def __init__(self, payload=None, target=None, parent_hash=None, gossip_hash=None, timestamp=None, owner=None):
        self.payload = payload
        self.target = target
        self.parent_hash = parent_hash
        self.gossip_hash = gossip_hash
        self.timestamp = timestamp
        self.owner = owner

class Event():

    def __init__(self, data=None, hashgraph=[]):
        self.graph = hashgraph
        self.visible_ancestors = set()
        self.invisible_ancestors = set()
        self.visible_hashes = set()
        self.invisible_hashes = set()
        self.data = data
        self.parent = None
        self.gossip_parent = None
        self.consensus_ts = (- 1)
        self.round_rcvd = (- 1)
        self.round = 0
        self.famous = (- 1)
        self.hash = hashlib.md5(str(self).encode('utf-8')).hexdigest()
        if (self.data.parent_hash == '\x00'):
            self.witness = True
        else:
            self.witness = False

    def ancestor_recursion(self, other_event, done, visited):
        if done:
            return done
        if (self == other_event):
            done = True
            return done
        if (self in visited):
            return False
        visited.append(self)
        if (self.data.timestamp < other_event.data.timestamp):
            return False
        if ((not self.parent) or (not self.gossip_parent)):
            return False
        return (self.parent.ancestor_recursion(other_event, done, visited) or self.gossip_parent.ancestor_recursion(other_event, done, visited))

    def event_ancestor(self, other_event):
        b = None
        visited = []
        done = False
        other_event_hash = other_event.hash
        if ((other_event_hash in self.visible_hashes) or (other_event_hash in self.visible_ancestors)):
            return True
        if (other_event_hash in self.invisible_ancestors):
            return False
        b = self.ancestor_recursion(other_event, done, visited)
        if (not b):
            self.invisible_ancestors.add(other_event_hash)
        else:
            self.visible_ancestors.add(other_event_hash)
        return b

class Person(da.DistProcess):

    def __init__(self, procimpl, props):
        super().__init__(procimpl, props)
        self._PersonReceivedEvent_1 = []
        self._PersonReceivedEvent_2 = []
        self._PersonReceivedEvent_3 = []
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_PersonReceivedEvent_0', PatternExpr_1812, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Person_handler_1811]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PersonReceivedEvent_1', PatternExpr_2017, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PersonReceivedEvent_2', PatternExpr_2037, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PersonReceivedEvent_3', PatternExpr_2065, sources=[PatternExpr_2070], destinations=None, timestamps=None, record_history=True, handlers=[])])

    def setup(self, s, nrequests, person_id, process_mapper, **rest_2199):
        super().setup(s=s, nrequests=nrequests, person_id=person_id, process_mapper=process_mapper, **rest_2199)
        self._state.s = s
        self._state.nrequests = nrequests
        self._state.person_id = person_id
        self._state.process_mapper = process_mapper
        self._state.others = self._state.s
        self._state.person_index = self._state.process_mapper[self._state.person_id]
        self._state.finished_nodes = list()
        self._state.current_round = 0
        init_data = Data(payload=0, target=(- 1), parent_hash='\x00', gossip_hash='\x00', timestamp=0, owner=self._state.person_index)
        init_event = Event(init_data)
        self._state.hashgraph = [init_event]
        self.output('INIT :', self._state.hashgraph, self._state.person_index)
        self._state.run_time = 0
        self._state.gossip_count = 1

    def run(self):

        def task():
            self.output('%s Process started', self._id)
        for i in range(self._state.nrequests):
            rand_list = random.sample(self._state.others, 1)
            rand_process = rand_list[0]
            self._state.run_time += 1
            self.gossip(self._state.run_time, rand_process)
            super()._label('_st_label_2014', block=False)
            _st_label_2014 = 0
            self._timer_start()
            while (_st_label_2014 == 0):
                _st_label_2014 += 1
                if PatternExpr_2022.match_iter(self._PersonReceivedEvent_1, SELF_ID=self._id):
                    _st_label_2014 += 1
                elif self._timer_expired:
                    _st_label_2014 += 1
                else:
                    super()._label('_st_label_2014', block=True, timeout=3)
                    _st_label_2014 -= 1
            else:
                if (_st_label_2014 != 2):
                    continue
            if (_st_label_2014 != 2):
                break
        super()._label('_st_label_2034', block=False)
        _st_label_2034 = 0
        self._timer_start()
        while (_st_label_2034 == 0):
            _st_label_2034 += 1
            if PatternExpr_2042.match_iter(self._PersonReceivedEvent_2, SELF_ID=self._id):
                _st_label_2034 += 1
            elif self._timer_expired:
                _st_label_2034 += 1
            else:
                super()._label('_st_label_2034', block=True, timeout=3)
                _st_label_2034 -= 1
        '\n        1. Create event at my end\n            Create contains a send - set\n        2. gossipee receive method\n            if what I have received contains\n            create an event\n            processing - adding things to my hashgraph\n            save it\n\n        will contain sending a message to the process\n        Each process will have a data structure\n        the DS contains the hashgraph\n        '
        self.send(('done', self._id), to=self.parent())
        super()._label('_st_label_2062', block=False)
        _st_label_2062 = 0
        while (_st_label_2062 == 0):
            _st_label_2062 += 1
            if PatternExpr_2073.match_iter(self._PersonReceivedEvent_3, _BoundPattern2079_=self.parent(), SELF_ID=self._id):
                _st_label_2062 += 1
            else:
                super()._label('_st_label_2062', block=True)
                _st_label_2062 -= 1
        self.output('terminating')

    def decide_fame(self, d_event):
        if ((not d_event.witness) or (d_event.round < 2)):
            return
        for each_event in d_event.graph[(- 1)::(- 1)]:
            if ((each_event.witness == True) and (each_event.famous == (- 1)) and (each_event.round <= (d_event.round - 2))):
                witnesses = self.find_witness((each_event.round + 1))
                count = 0
                count_num = 0
                remove_list = list()
                for (index, each_witness) in enumerate(witnesses):
                    if (not self.event_strongly_visible(d_event, each_witness)):
                        remove_list.append(index)
                    elif self.event_visible(each_witness, each_event):
                        count += 1
                    else:
                        count_num += 1
                diff = (d_event.round - each_event.round)
                if (count > ((2 * len(self._state.process_mapper)) / 3)):
                    each_event.famous = 1
                elif (count > ((2 * len(self._state.process_mapper)) / 3)):
                    each_event.famous = 0

    def findUFW(self, witnesses):
        event_list = ([None] * (len(self._state.others) + 1))
        fame_list = ([0] * (len(self._state.others) + 1))
        for witness in witnesses:
            if (witness.famous == True):
                pid = witness.data.owner
                if (fame_list[pid] == 1):
                    fame_list[pid] = (- 1)
                    event_list[pid] = None
                if (fame_list[pid] == 0):
                    fame_list[pid] = 1
                    event_list[pid] = witness
        return event_list

    def create_event(self, time, gossiper):
        new_data = Data(payload=0, target=(- 1))
        if (not random.randint(0, 9)):
            new_data.payload = random.randint(0, 99999)
            new_data.target = random.randint(0, (len(self._state.others) + 1))
        new_data.owner = self._state.person_index
        self_top_event = None
        try:
            self_top_event = max(list(filter((lambda x: (x.data.owner == self._state.person_index)), self._state.hashgraph)), key=(lambda x: x.data.timestamp))
        except Exception as e:
            print(e)
            pass
        if self_top_event:
            new_data.parent_hash = self_top_event.hash
        else:
            new_data.parent_hash = '\x00'
        gossiper_top_event = None
        try:
            gossiper_top_event = max(list(filter((lambda x: (x.data.owner == self._state.process_mapper[gossiper.uid])), self._state.hashgraph)), key=(lambda x: x.data.timestamp))
        except Exception as e:
            print(e)
            pass
        if gossiper_top_event:
            new_data.gossip_hash = gossiper_top_event.hash
        else:
            new_data.gossip_hash = '\x00'
        new_data.timestamp = time
        new_event = Event(new_data, self._state.hashgraph)
        self._state.hashgraph.insert(0, new_event)

    def finalize_order(self, n, r, witnesses):
        ufw = self.findUFW(witnesses)
        j = 0
        self._state.s = []
        while ((j < (len(self._state.others) + 1)) and ((not ufw[j]) or ufw[j].ancestor(self._state.hashgraph[n]))):
            j += 1
        if (j == (len(self._state.others) + 1)):
            for i in range((len(self._state.others) + 1)):
                if ufw[i]:
                    tmp = ufw[i]
                    while (tmp.parent and tmp.parent.ancestor(self._state.hashgraph[n])):
                        tmp = tmp.parent
                    self._state.s.append(tmp.data.timestamp)
            if (not len(self._state.s)):
                return 1
            self._state.hashgraph[n].round_rcvd = r
            self._state.s.sort()
            self._state.hashgraph[n].data.timestamp = self._state.s[(len(self._state.s) // 2)]

    def find_order(self):
        witness = []
        for each_event in self._state.hashgraph[(- 1)::(- 1)]:
            if (each_event.round == (- 1)):
                for i in range(each_event.round, self._state.hashgraph[0].round):
                    witness = self.find_witness(i)
                    j = 0
                    while ((j < len(witness)) and (not (witness[j].famous == (- 1)))):
                        j += 1
                    if (j == len(witness)):
                        if self.finalize_order(len(self._state.hashgraph), i, witness):
                            break

    def remove_old_events(self):
        remove_list = []
        for (index_event, each_event) in enumerate(self._state.hashgraph):
            if ((not (each_event.consensus_ts == (- 1))) and (each_event.witness == False)):
                self._state.finished_nodes.append(each_event)
                remove_list.append(index_event)
            if (each_event.round < (self._state.current_round - 5)):
                remove_list.append(index_event)
        for ind in remove_list:
            self._state.hashgraph.pop(ind)
        for (node_index, node) in enumerate(self._state.finished_nodes):
            if (node.round < (self._state.current_round - 6)):
                remove_list.append(node_index)
        for ind in remove_list:
            self._state.finished_nodes.pop(ind)

    def link_events(self, events):
        for each_event in events:
            if ((each_event.parent == None) and (not (each_event.data.parent_hash == '\x00'))):
                c = 0
                for x_event in self._state.hashgraph:
                    if (x_event.hash == each_event.data.parent_hash):
                        each_event.parent = x_event
                        c += 1
                        if (c == 2):
                            break
                    if (x_event.hash == each_event.data.gossip_hash):
                        each_event.gossip_parent = x_event
                        c += 1
                        if (c == 2):
                            break

    def recursive_visibility_check(self, e_events, other_event, visited):
        if (not e_events):
            return False
        if (e_events in visited):
            return False
        visited.append(e_events)
        if (e_events == other_event):
            return True
        if (e_events.data.timestamp < other_event.data.timestamp):
            return False
        ' if not e_events.parent:\n            return False '
        return (self.recursive_visibility_check(e_events.parent, other_event, visited) or self.recursive_visibility_check(e_events.gossip_parent, other_event, visited))

    def event_visible(self, e_events, other_event):
        if (other_event.hash in e_events.visible_hashes):
            return True
        if (other_event.hash in e_events.invisible_hashes):
            return False
        visited = []
        done = self.recursive_visibility_check(e_events, other_event, visited)
        if (not done):
            e_events.invisible_hashes.add(other_event.hash)
            return False
        e_events.visible_hashes.add(other_event.hash)
        return True

    def event_strongly_visible(self, e_event, other_event):
        num_visible = 0
        found = ([False] * len(self._state.process_mapper))
        for g in e_event.graph:
            if ((found[g.data.owner] == True) or (g.round < other_event.round)):
                continue
            if self.event_visible(e_event, g):
                if self.event_visible(g, other_event):
                    num_visible += 1
                    found[g.data.owner] = True
                    if (num_visible >= ((2 * len(self._state.process_mapper)) / 3)):
                        return True
        return False

    def find_witness(self, w_round):
        size = len(self._state.hashgraph)
        witnesses = []
        hashref = copy.deepcopy(self._state.hashgraph)
        for i in range(size):
            if (hashref[i].round < (w_round - 1)):
                break
            if ((hashref[i].round == w_round) and (hashref[i].witness == True)):
                witnesses.append(hashref[i])
        return witnesses

    def divide_rounds(self, d_event):
        if ((not d_event.parent) or (not d_event.gossip_parent)):
            d_event.round = 0
            self.output('Round: ', d_event.round, d_event.data.owner)
            return
        d_event.round = d_event.parent.round
        if (d_event.gossip_parent.round > d_event.round):
            d_event.round = d_event.gossip_parent.round
        num_strong_visibility = 0
        witnesses = self.find_witness(d_event.round)
        for each_witness in witnesses:
            if (num_strong_visibility >= ((2 * len(self._state.process_mapper)) / 3)):
                break
            if self.event_strongly_visible(d_event, each_witness):
                num_strong_visibility += 1
        if (num_strong_visibility >= ((2 * len(self._state.process_mapper)) / 3)):
            d_event.round += 1
            if (self._state.current_round < d_event.round):
                self._state.current_round += 1
        d_event.witness = ((d_event.parent == None) or (d_event.parent.round < d_event.round))
        self.output('Round: ', d_event.round, d_event.data.owner)

    def gossip(self, run_time, rand_person):
        gossip_data = []
        self._state.hashgraph.sort(key=(lambda x: x.data.timestamp), reverse=True)
        rand_person_top_event = None
        try:
            rand_person_top_event = max(list(filter((lambda x: (x.data.owner == self._state.process_mapper[rand_person.uid])), self._state.hashgraph)), key=(lambda x: x.data.timestamp))
        except Exception:
            pass
        person_seen = ([False] * (len(self._state.others) + 1))
        for each_event in self._state.hashgraph:
            if (person_seen[each_event.data.owner] == False):
                if (rand_person_top_event and self.event_visible(rand_person_top_event, each_event)):
                    person_seen[each_event.data.owner] = True
                gossip_data.append(each_event.data)
        gossip_listener = set()
        gossip_listener.add(rand_person)
        self.send(('gossip', gossip_data, run_time, self._id), to=gossip_listener)
        self.output('Gossip sent to ', self._state.process_mapper[rand_person.uid], 'from', self._state.person_index)
        " await(received(('gossip_rcvd',)), timeout = 3) "

    def _Person_handler_1811(self, gossip_data, run_time, gossiper):
        self.output('Gossip Received by ', self._state.person_index, 'from', self._state.process_mapper[gossiper.uid])
        events = []
        for each_data in gossip_data:
            new_event = str(Event(each_data, self._state.hashgraph)).encode('utf-8')
            event_hash = hashlib.md5(new_event)
            if (not list(filter((lambda x: (x.hash == event_hash)), self._state.hashgraph))):
                temp_event = Event(each_data, self._state.hashgraph)
                self._state.hashgraph.insert(0, temp_event)
                events.append(temp_event)
        self.create_event(run_time, gossiper)
        self.output('New event created on hearing the gossip')
        events.append(self._state.hashgraph[0])
        events.sort(key=(lambda x: x.data.timestamp))
        self.link_events(events)
        self.output('DEBUG - Events linked')
        for each_event in events:
            self.divide_rounds(each_event)
        self.output('DEBUG - Divided into rounds')
        self.remove_old_events()
        self.output('DEBUG - Old events removed')
        for each_event in events:
            self.decide_fame(each_event)
        self.find_order()
        self.send(('gossip_rcvd',), to=gossiper)
    _Person_handler_1811._labels = None
    _Person_handler_1811._notlabels = None

class Node_(da.NodeProcess):

    def __init__(self, procimpl, props):
        super().__init__(procimpl, props)
        self._Node_ReceivedEvent_0 = []
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_Node_ReceivedEvent_0', PatternExpr_2177, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[])])

    def run(self):
        nprocs = (int(sys.argv[1]) if (len(sys.argv) > 1) else 3)
        nrequests = (int(sys.argv[2]) if (len(sys.argv) > 2) else 5)
        ps = self.new(Person, num=nprocs)
        for (index, p) in enumerate(ps):
            process_mapper[p.uid] = index
        for (index, p) in enumerate(ps):
            self._setup(p, ((ps - {p}), nrequests, p.uid, process_mapper))
        self._start(ps)
        super()._label('_st_label_2169', block=False)
        p = None

        def UniversalOpExpr_2170():
            nonlocal p
            for p in ps:
                if (not PatternExpr_2183.match_iter(self._Node_ReceivedEvent_0, _BoundPattern2193_=p)):
                    return False
            return True
        _st_label_2169 = 0
        while (_st_label_2169 == 0):
            _st_label_2169 += 1
            if UniversalOpExpr_2170():
                _st_label_2169 += 1
            else:
                super()._label('_st_label_2169', block=True)
                _st_label_2169 -= 1
        self.send(('done',), to=ps)
